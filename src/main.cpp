#include <stdio.h>
#include <stdlib.h>
#include <SimpleButton.h>
#include "stm32f10x.h"
#include "bluepill.h"
#include "millis.h"
#include "micros.h"
#include "Led.h"
#include "stm32f10x_rtc.h"
#include "Pin.h"
#include "I2C.h"
#include "stm32f10x_dma.h"
#include "Serial.h"
#include "timers.h"
#include "misc.h"
#include "DS3231.h"
#include "SK6812Strip.h"

#include "OledDigit.h"
#include "I2CMultiplexer.h"
#include "PoltaxDisplay.h"




void time_manager(void);
void display_manager(void);
void esp8266_manager(void);
void buttons_manager(void);
void manager_1(void);
void manager_2(void);
void update_time(void);

void i2c_write_command(I2C i2c, uint8_t command, uint8_t address);
void copy_frame(unsigned char target_frame[], const unsigned char source_frame[]);

Led 	pc13_led(PC13, Bit_RESET);


#define DISP_NUMBER_OF_DIGITS			4
#define DISP_DEFAULT_BRIGHTNESS 		20
#define VINTAGE_BACKLIGHT					0x99FF11

I2C	display_i2c(I2C2);
I2CMultiplexer	multiplex(&display_i2c);
PoltaxDisplay	display(
		&display_i2c,
		&multiplex,
		DISP_NUMBER_OF_DIGITS,
		DISP_DEFAULT_BRIGHTNESS);

SK6812Strip led_strip(3, SPI2);

DS3231	dallas_rtc(I2C1);

Serial	esp8266_serial(USART1);
Pin		esp8266_reset_pin(PB14, Bit_RESET);

SimpleButton hours_up(PA0, Bit_RESET, DISABLE);
SimpleButton hours_down(PA1, Bit_RESET, DISABLE);
SimpleButton minutes_up(PA2, Bit_RESET, DISABLE);
SimpleButton minutes_down(PB0, Bit_RESET, DISABLE);
SimpleButton color_change(PB1, Bit_RESET, DISABLE);

Timestamp dallas_time;


struct main_time_struct {
	uint8_t time[4] = {1, 2, 3, 4};
	bool is_motion_detected = true;
	uint32_t last_motion_detected_timestamp = 0;
	bool blank_rtc_leading_zero = true;
	uint32_t millis_at_last_sync = 0;
	bool time_sync_request = true;
} main_time_structure;


u32 light_colors_table[] ={
		WHITE,
		BLUE,
		RED,
		AQUA,
		YELLOW,
		GREEN,
		PURPLE,
		VINTAGE_BACKLIGHT,
		OFF,
};
u8 current_color = 0;


void main() {

	//init
	NVIC_PriorityGroupConfig(NVIC_PriorityGroup_4);
	millis_init();
	adc_random_init();

    pc13_led.turn_off();

    led_strip.set_color(VINTAGE_BACKLIGHT);
//    led_strip.set_color(0x000000);

	esp8266_reset_pin.digital_write(Bit_SET);

	Timers<8> timers;
	timers.attach(0, 5000, time_manager);
	timers.attach(1, 1000, display_manager);
	timers.attach(2, 1000, esp8266_manager);
	timers.attach(3, 50, buttons_manager);
//	timers.attach(6, 1000, manager_1, 0);
//	timers.attach(7, 1666, manager_2, 0);

	while(1) {
		timers.process();
	}
} //main()


void time_manager(void) {
	dallas_time = dallas_rtc.get_time();

	//update display digits
	main_time_structure.time[0] = (uint8_t)  dallas_time.hours / 10;
	main_time_structure.time[1] = (uint8_t)  dallas_time.hours % 10;
	main_time_structure.time[2] = (uint8_t)  dallas_time.minutes / 10;
	main_time_structure.time[3] = (uint8_t)  dallas_time.minutes % 10;
} //time_manager()


void display_manager(void) {
	display.show(main_time_structure.time);
} //display_manager()


void buttons_manager(void) {
	static BitAction minutes_up_button_last_state = Bit_SET;
	static BitAction minutes_down_button_last_state = Bit_SET;
	static BitAction hours_up_button_last_state = Bit_SET;
	static BitAction hours_down_button_last_state = Bit_SET;
	static BitAction color_change_button_last_state = Bit_SET;

	if (hours_up.is_button_pressed() == true) {
		if (hours_up_button_last_state == Bit_SET) {
			hours_up_button_last_state = Bit_RESET;
			dallas_rtc.hours_increment();
			if (main_time_structure.time[1] == 9) {
				main_time_structure.time[1] = 0;
				main_time_structure.time[0]++;
			} else if ((main_time_structure.time[0] == 2) && (main_time_structure.time[1] == 3)) {
				main_time_structure.time[0] = 0;
				main_time_structure.time[1] = 0;
			} else {
				main_time_structure.time[1]++;
			}
			display_manager();
		}
	} else {
		hours_up_button_last_state = Bit_SET;
	}

	if (hours_down.is_button_pressed() == true) {
		if (hours_down_button_last_state == Bit_SET) {
			hours_down_button_last_state = Bit_RESET;
			dallas_rtc.hours_decrement();
			if (main_time_structure.time[1] == 0) {
				if (main_time_structure.time[0] == 0) {
					main_time_structure.time[0] = 2;
					main_time_structure.time[1] = 3;
				} else {
					main_time_structure.time[0]--;
					main_time_structure.time[1] = 9;
				}
			} else {
				main_time_structure.time[1]--;
			}
			display_manager();
		}
	} else {
		hours_down_button_last_state = Bit_SET;
	}

	if (minutes_up.is_button_pressed() == true) {
		if (minutes_up_button_last_state == Bit_SET) {
			minutes_up_button_last_state = Bit_RESET;
			dallas_rtc.minutes_increment();
			if (main_time_structure.time[3] == 9) {
				main_time_structure.time[3] = 0;
				if (main_time_structure.time[2] == 5) {
					main_time_structure.time[2] = 0;
				} else {
					main_time_structure.time[2]++;
				}
			} else {
				main_time_structure.time[3]++;
			}
			display_manager();
		}
	} else {
		minutes_up_button_last_state = Bit_SET;
	}

	if (minutes_down.is_button_pressed() == true) {
		if (minutes_down_button_last_state == Bit_SET) {
			minutes_down_button_last_state = Bit_RESET;
			dallas_rtc.minutes_decrement();
			if (main_time_structure.time[3] == 0) {
				main_time_structure.time[3] = 9;
				if (main_time_structure.time[2] == 0) {
					main_time_structure.time[2] = 5;
				} else {
					main_time_structure.time[2]--;
				}
			} else {
				main_time_structure.time[3]--;
			}
			display_manager();
		}
	} else {
		minutes_down_button_last_state = Bit_SET;
	}

	if (color_change.is_button_pressed() == true) {
		if (color_change_button_last_state == Bit_SET) {
			color_change_button_last_state = Bit_RESET;


			if (++current_color >= 9) {
				current_color = 0;
			}
			led_strip.set_color(light_colors_table[current_color]);
		}
	} else {
		color_change_button_last_state = Bit_SET;
	}
} //buttons_manager()

void manager_1(void) {
	led_strip.set_color(OFF);
} //manager_1()

void manager_2(void) {
	led_strip.set_color(WHITE);
} //manager_2()


void esp8266_manager(void) {
	if (main_time_structure.time_sync_request == true) {
		if (esp8266_serial.is_rx_busy == false) {
			esp8266_serial.send_it("time_request\n");
			esp8266_serial.receive_it_with_callback(update_time);
		}
	}
} //esp8266_manager()


void update_time(void) {
	Timestamp time;

	if (esp8266_serial.rx_buf[0] == '!') {
		time.hours = (esp8266_serial.rx_buf[2]-0x30)*10 + (esp8266_serial.rx_buf[3]-0x30);
		time.minutes = (esp8266_serial.rx_buf[5]-0x30)*10 + (esp8266_serial.rx_buf[6]-0x30);
		time.seconds = (esp8266_serial.rx_buf[8]-0x30)*10 + (esp8266_serial.rx_buf[9]-0x30);
		time.time_format = TIME_FORMAT_24H;
		dallas_rtc.set_time(time);

		main_time_structure.millis_at_last_sync = millis();
		main_time_structure.time_sync_request = false;
	}
} //update_time()
