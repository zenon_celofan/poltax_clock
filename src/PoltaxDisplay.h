#ifndef POLTAXDISPLAY_H_
#define POLTAXDISPLAY_H_

#include "OledDigit.h"
#include "I2C.h"
#include "I2CMultiplexer.h"

#define PTX_NUMBER_OF_DIGITS		(4)


class PoltaxDisplay {

public:

	I2C * 			 i2c;
	I2CMultiplexer * i2c_multiplexer;
	u8 				 number_of_digits;
	OledDigit ** 	 digit;
	bool 			 turned_on;
	u8				 brightness;

	PoltaxDisplay(
			I2C * _i2c,
			I2CMultiplexer * _i2c_multiplexer,
			u8 _number_of_digits = PTX_NUMBER_OF_DIGITS,
			u8 _brightness_percent = 50);
	void refresh(void);
	void turn_on(void);
	void turn_off(void);

	void show(u8 * _digits);
};

#endif /* POLTAXDISPLAY_H_ */
