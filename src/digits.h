#ifndef DIGITS_H_
#define DIGITS_H_

#include "bluepill.h"

#define OLED_DIGIT_HEIGHT		68
#define OLED_DIGIT_BYTES		544

extern const uint8_t digit_table[];

#endif /* DIGITS_H_ */
