#ifndef I2CMULTIPLEXER_H_
#define I2CMULTIPLEXER_H_

#include "I2C.h"

#define MUL_MULTIPLEXER_ADDR	(0xE0)	// 0x70 << 1 (incl. R/W bit)


class I2CMultiplexer {

public:

	u8 i2c_address;
	s8 active_channel;	// -1 when all channels are OFF
	I2C * i2c;

	I2CMultiplexer(I2C * _i2c, u8 _i2c_address = MUL_MULTIPLEXER_ADDR);
	void activate_channel(u8 _channel_number);
	void deactivate_all_channels(void);
};

#endif /* I2CMULTIPLEXER_H_ */
