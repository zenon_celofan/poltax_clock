#include "OledDigit.h"

#include "bluepill.h"
#include "millis.h"
#include "digits.h"
#include <stdlib.h>



OledDigit::OledDigit(I2C * _i2c, bool _rotate_display, u8 _brightness_percent)
: i2c(_i2c)
, digit(0)
, current_line(0)
, step(0)
, digit_ready(false) {
	brightness = (u8) _brightness_percent * 0xFF / 100;
	init(_rotate_display);
} //OledDigit()


void OledDigit::init(bool _rotate_display) {
	charge_pump_off();
	display_disable();
	charge_pump_on();
	set_clock_divider(0x80);
	set_inverse_colors(false);
	set_brightness(brightness);
	set_addresing_mode(OLED_Vertical_Mode);
	set_segment_remap(true);   // false  --
	set_com_remap(false);  // true
	set_col_start_and_end_addr(0, 127);  // 0,127
	set_page_start_and_end_addr(0, 7);  // 0, 7
	set_display_ram_start_line(0);  //0
	set_vertical_shift(0);
	send_command(OLED_Dectivate_Scroll_Cmd);
	display_enable();

	display_ram_contents();
} //init()


void OledDigit::send_command(const uint8_t command) {
	uint8_t buff[] = {OLED_Command_Mode, command};
	i2c->master_write(buff, 2, OLED_Address);
} //send_command()


void OledDigit::send_data(const uint8_t byte) {
	uint8_t buff[] = {OLED_Data_Mode, byte};
	i2c->master_write(buff, 2, OLED_Address);
} //send_data()


void OledDigit::send_data(const uint8_t * data, uint16_t length) {
	i2c->master_write(OLED_Data_Mode, data, length, OLED_Address);
} //send_data()


void OledDigit::charge_pump_on(void) {
	send_command(0x8D);
	send_command(0x14);
} //charge_pump_on()


void OledDigit::charge_pump_off(void) {
	send_command(0x8D);
	send_command(0x10);
} //charge_pump_off()


void OledDigit::display_enable(void) {
	send_command(OLED_Display_On_Cmd);  //0xAF
} //display_enable()


void OledDigit::display_disable(void) {
	send_command(OLED_Display_Off_Cmd);  //0xAE
} //display_disable()


void OledDigit::clear() {
	uint8_t black_screen[OLED_SCREEN_SIZE] = {0};

	home_carriage();
	i2c->master_write(OLED_Data_Mode, (uint8_t*) black_screen, OLED_SCREEN_SIZE, OLED_Address);
} //clear()


void OledDigit::refresh() {
	/*
	 * 	1 byte of data sent to display holds 4 pixels:
	 * 	Data byte: 0bDdCcBbAa   (a, A, b, B, c, C, d, D could be 1 (white) or 0 (black))
	 *  Com Remap Disabled: 	Displayed pixels: ABCD
	 *  Com Remap Enabled:	 	Displayed pixels: abcd
	 */

	if (digit_ready == false) {
		update_screen();
		home_carriage();

		i2c->master_write(OLED_Data_Mode, (uint8_t*) digit_table+current_line, OLED_SCREEN_SIZE, OLED_Address);
	}
} //refresh()


void OledDigit::home_carriage(void) {
	send_command(0xB0); //0xB0
	send_command(0x00); //0x00
	send_command(0x10); //0x10
} //set_display_offset()


void OledDigit::display_number(uint8_t number_to_display) {
	digit = number_to_display;
	update_screen();
} //update_screen()


void OledDigit::display_number_uninteruptable(uint8_t number_to_display) {
	digit = number_to_display;
	do {
		update_screen();
		refresh();
	} while (digit_ready != true);
} //display_number_uninteruptable()


void OledDigit::update_screen(void) {
	int16_t number_line;

	if (current_line == digit * OLED_DIGIT_BYTES) {
		step = 0;
		digit_ready = true;
		return;
	} else {
		digit_ready = false;
	}

	if ((current_line > 5*OLED_DIGIT_BYTES) and (digit == 0)) {
		number_line = 10*OLED_DIGIT_BYTES;
	} else {
		number_line = uint16_t(OLED_DIGIT_BYTES * digit);
	}


	if (current_line < number_line) {
		if (number_line - current_line < 5*OLED_DIGIT_BYTES) {
			if (number_line - current_line < (int32_t) consecutive_numbers_sum(uint16_t(abs(step)+1)) * OLED_BYTES_PER_LINE) {
				if (step > 1) {
					step--;
				}
			} else if (step < OLED_MAX_STEP) {
				step++;
			}
		} else {
			if (step > -OLED_MAX_STEP) {
				step--;
			}
		}
	} else {
		if (current_line - number_line <= 5*OLED_DIGIT_BYTES) {
			if (current_line - number_line < (int32_t) consecutive_numbers_sum(uint16_t(abs(step)+1)) * OLED_BYTES_PER_LINE) {
				if (step < -1) {
					step++;
				}
			} else if (step > -OLED_MAX_STEP) {
				step--;
			}
		} else {
			if (step < OLED_MAX_STEP) {
				step++;
			}
		}
	}

	current_line += step * int16_t(OLED_BYTES_PER_LINE);
	if (current_line < 0) {
		current_line += 10*OLED_DIGIT_BYTES;
	} else if (current_line > 10*OLED_DIGIT_BYTES) {
		current_line -= 10*OLED_DIGIT_BYTES;
	}

	if (current_line == 10*OLED_DIGIT_BYTES) {
		current_line = 0;
	}
} //update_screen()


void OledDigit::activate_scroll() {
    send_command(OLED_Activate_Scroll_Cmd);
}

void OledDigit::deactivate_scroll() {
    send_command(OLED_Dectivate_Scroll_Cmd);
}


void OledDigit::set_mux_ratio(uint8_t ratio) {
	send_command(OLED_Set_Mux_Ratio_Cmd);
	send_command(ratio);
} //set_mux_ratio()


void OledDigit::set_vertical_shift(uint8_t pixels) {
	send_command(OLED_Set_Vertical_Shift_Cmd);
	send_command(pixels);
} //set_vertical_shift()


void OledDigit::set_display_ram_start_line(uint8_t line) {
	send_command((uint8_t) 0x40 + line);
} //set_display_ram_start_line()


void OledDigit::set_segment_remap(bool remap_enable) {
	if (remap_enable == false) {
		send_command(OLED_Set_Seg_Remap_Disable_Cmd);
	} else {
		send_command(OLED_Set_Seg_Remap_Enable_Cmd);
	}
} //set_segment_remap()


void OledDigit::set_com_remap(bool remap_enable) {
	if (remap_enable == false) {
		send_command(OLED_Set_Com_Remap_Disable_Cmd);
	} else {
		send_command(OLED_Set_Com_Remap_Enable_Cmd);
	}
} //set_com_remap()


void OledDigit::set_brightness(uint8_t brightness) {
	send_command(OLED_Set_Brightness_Cmd);
	send_command(brightness);
} //set_contrast()


void OledDigit::all_pixels_on(void) {
	send_command(OLED_All_Pixels_On_Cmd);
} //all_pixels_on()


void OledDigit::display_ram_contents(void) {
	send_command(OLED_Display_Ram_Contents_Cmd);
} //display_ram_contents()


void OledDigit::set_inverse_colors(bool inversed_colors) {
	if (inversed_colors == false) {
		send_command(OLED_Normal_Display_Cmd);
	} else {
		send_command(OLED_Inverse_Display_Cmd);
	}
} //set_inverse_colors()


void OledDigit::set_clock_divider(uint8_t divider) {
	send_command(OLED_Set_Clock_Divider_Cmd);
	send_command(divider);
} //set_clock_divider()


void OledDigit::set_addresing_mode(uint8_t mode) {
	send_command(OLED_Set_Addr_Mode_Cmd);
	send_command(mode);
} //set_addresing_mode()


void OledDigit::set_col_start_and_end_addr(uint8_t _start_addr, uint8_t _end_addr) {
	send_command(OLED_Set_Col_Start_End_Addr_Cmd);
	send_command(_start_addr);
	send_command(_end_addr);
} //set_col_start_and_end_addr()


void OledDigit::set_page_start_and_end_addr(uint8_t _start_addr, uint8_t _end_addr) {
	send_command(OLED_Set_Page_Start_End_Addr_Cmd);
	send_command(_start_addr);
	send_command(_end_addr);
} //set_page_start_and_end_addr()
