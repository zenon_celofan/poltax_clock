#include "PoltaxDisplay.h"
#include "I2C.h"
#include "OledDigit.h"

PoltaxDisplay::PoltaxDisplay(
		I2C * _i2c,
		I2CMultiplexer * _i2c_multiplexer,
		u8 _number_of_digits,
		u8 _brightness_percent)
: i2c(_i2c)
, i2c_multiplexer(_i2c_multiplexer)
, number_of_digits(_number_of_digits)
, digit(new OledDigit*[_number_of_digits])
, turned_on(false)
, brightness(_brightness_percent) {
	for (u8 i = 0; i < number_of_digits; ++i) {
		i2c_multiplexer->activate_channel((u8) i+2);
		digit[i] = new OledDigit(i2c, false, _brightness_percent);
		digit[i]->display_number_uninteruptable(0);
	}
} //constructor


void PoltaxDisplay::show(u8 * _digits) {
	for (s8 i = number_of_digits-1; i >= 0; --i) {
		i2c_multiplexer->activate_channel((u8) 2 + i);
		digit[i]->display_number_uninteruptable(_digits[i]);
	}
} //show()
