#ifndef OLED_DIGIT_H
#define OLED_DIGIT_H

#include "bluepill.h"
#include "I2C.h"

#define OLED_Address               		0x78
#define OLED_Command_Mode          		0x80
#define OLED_Data_Mode           		0x40
#define OLED_Continuation_Bit			0b10000000

#define OLED_Horizontal_Mode			0x00
#define OLED_Vertical_Mode              0x01
#define OLED_Page_Mode					0x02

#define OLED_Display_Off_Cmd      		0xAE
#define OLED_Display_On_Cmd      		0xAF

#define OLED_Display_Ram_Contents_Cmd	0xA4
#define OLED_All_Pixels_On_Cmd			0xA5

#define OLED_Normal_Display_Cmd   		0xA6
#define OLED_Inverse_Display_Cmd  		0xA7

#define OLED_Activate_Scroll_Cmd   		0x2F
#define OLED_Dectivate_Scroll_Cmd  		0x2E

#define OLED_Set_Brightness_Cmd    		0x81
#define OLED_Set_Mux_Ratio_Cmd			0xA8
#define OLED_Set_Vertical_Shift_Cmd		0xD3
#define OLED_Set_Clock_Divider_Cmd		0xD5
#define OLED_Set_Seg_Remap_Disable_Cmd	0xA0
#define OLED_Set_Seg_Remap_Enable_Cmd	0xA1
#define OLED_Set_Com_Remap_Disable_Cmd	0xC0
#define OLED_Set_Com_Remap_Enable_Cmd	0xC8
#define OLED_MUX64						0x3F
#define OLED_Set_Addr_Mode_Cmd			0x20
#define OLED_Set_Col_Start_End_Addr_Cmd 0x21
#define OLED_Set_Page_Start_End_Addr_Cmd	0x22

#define OLED_MAX_STEP					5	//max scroll speed

#define OLED_BYTES_PER_LINE				8
#define OLED_SCREEN_SIZE				1024  //128 lines * 8 bytes per line

extern const uint8_t digit_table[];


class OledDigit {

public:

	I2C * i2c;
	uint8_t digit;
	int16_t current_line;
	int8_t	step;
	bool digit_ready;
	u8 brightness;


	OledDigit(I2C * _i2c, bool _rotate_display = false, u8 _brightness_percent = 50);
    void init(bool _rotate_display);

    void send_command(const uint8_t command);
    void send_data(const uint8_t byte);
    void send_data(const uint8_t * data, uint16_t length);

    void charge_pump_on(void);
    void charge_pump_off(void);
    void display_enable(void);
    void display_disable(void);
    void clear(void);
    void refresh(void);
    void home_carriage(void);
    void display_number(uint8_t number_to_display);
    void display_number_uninteruptable(uint8_t number_to_display);
    void update_screen(void);
    void activate_scroll(void);
    void deactivate_scroll(void);

    void set_mux_ratio(uint8_t ratio);
    void set_vertical_shift(uint8_t pixels);
    void set_display_ram_start_line(uint8_t line);
    void set_segment_remap(bool remap_enable);
    void set_com_remap(bool remap_enable);
    void set_brightness(uint8_t brightness);
    void all_pixels_on(void);
    void display_ram_contents(void);
    void set_inverse_colors(bool inversed_colors);
    void set_clock_divider(uint8_t divider);
    void set_addresing_mode(uint8_t mode);
    void set_col_start_and_end_addr(uint8_t _start_addr, uint8_t _end_addr);
    void set_page_start_and_end_addr(uint8_t _start_addr, uint8_t _end_addr);
};

#endif
