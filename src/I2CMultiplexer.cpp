#include "I2CMultiplexer.h"
#include "I2C.h"


I2CMultiplexer::I2CMultiplexer(I2C * _i2c, u8 _i2c_address)
: i2c_address(_i2c_address)
, active_channel(-1)
, i2c(_i2c)
{
	deactivate_all_channels();
} //constructor()

void I2CMultiplexer::activate_channel(u8 _channel_number) {
	/*
	 * Activate channels by sending [slave_addr][command]
	 * [command] is 1 << n, where n is the number of channels to activate
	 * [command] == 0 to deactivate all channels
	 * more than one channel can be activated at a time
	 */
	u8 command = (u8) (1 << _channel_number);
	active_channel = _channel_number;
	i2c->master_write(&command, 1, i2c_address);
} //activate_channel()


void I2CMultiplexer::deactivate_all_channels() {
	u8 command = 0;
	active_channel = -1;
	i2c->master_write(&command, 1, i2c_address);
} //deactivate_all_channels()
