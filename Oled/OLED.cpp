#include "OLED.h"
#include "bluepill.h"
#include "millis.h"



Oled::Oled(I2C_TypeDef* _i2c) : i2c(_i2c) {
	adc_random_init();
	init();
//	current_frame = (unsigned char *) digit_4;
//	next_frame = (unsigned char *) digit_5;
} //Oled()


void Oled::init(void) {
	charge_pump_off();
	display_disable();
	charge_pump_on();
	set_clock_divider(0x80);
	set_inverse_colors(false);
	set_brightness(0x7F);
	set_addresing_mode(VERTICAL_MODE);
	set_segment_remap(false);
	set_com_remap(true);
//	set_start_page_addr()

	send_command(OLED_Dectivate_Scroll_Cmd);
	display_ram_contents();
	display_enable();
} //init()


void Oled::send_command(unsigned char command) {
	unsigned char buff[] = {OLED_Command_Mode, command};
	i2c.master_write(buff, 2, OLED_Address);
} //send_command()


//void Oled::set_pixel(unsigned char x, unsigned char y, unsigned char status) {
//	unsigned char col = x / 8;
//	unsigned char row = y;
//	if (status == 0) {
//		g_matrix[col][row] &= ~(1 << (7 - (x % 8)));
//	} else {
//		g_matrix[col][row] |= (1 << (7 - (x % 8)));
//	}
//}


void Oled::refresh() {

	// Home cursor
//	send_command(0xB0);
//	send_command(0x00);
//	send_command(0x10);

	/*
	 * 	1 byte of data sent to display holds 4 pixels:
	 * 	Pixels: ABCD
	 * 	Data byte: 0bDxCxBxAx   (A, B, C, D could be 1 (white) or 0 (black), x - don't care)
	 */
	i2c.master_write(current_frame, FRAME_SIZE, OLED_Address);
} //refresh()


void Oled::scroll_up(uint8_t step) {
	uint8_t * digit;
	uint8_t * next_digit;

	for (uint8_t k = 0; k < 128; k = k + step) {
		digit = &(current_frame[1]);
		next_digit = &(next_frame[1]);
		for (uint8_t s = 0; s < step; s++) {
			if ((k + s) < 128) {
				for (uint8_t i = 0; i < 127; i++) {
					for (uint8_t j = 0; j < 8; j++) {
						digit[i*8 + j] = digit[(i+1)*8 + j];
					}
				}
				for (uint8_t i = 0; i < 8; i++) {
					digit[8*127 + i] = next_digit[(k+s)*8+i];
				}
			} else {
				break;
			}
		}
		refresh();
	}
} //scroll_up()


void Oled::scroll_down(uint8_t step) {
	uint8_t * digit;
	uint8_t * next_digit;

	for (uint8_t k = 0; k < 128; k = k + step) {
		digit = &(current_frame[1]);
		next_digit = &(next_frame[1]);
		for (uint8_t s = 0; s < step; s++) {
			if ((k + s) < 128) {
				for (uint8_t i = 127; i > 0; i--) {
					for (uint8_t j = 0; j < 8; j++) {
						digit[i*8 + j] = digit[(i-1)*8 + j];
					}
				}
				for (uint8_t i = 0; i < 8; i++) {
					digit[i] = next_digit[(127-k-s)*8+i];
				}
			} else {
				break;
			}
		}
		refresh();
	}
} //scroll_down()


void Oled::scroll_random_direction(uint8_t step) {
	if (adc_random(2)) {
		scroll_up(step);
	} else {
		scroll_down(step);
	}
} //scroll_random_direction()


void Oled::clear_display() {
    unsigned char i, j;
    send_command(OLED_Display_Off_Cmd);   //display off
    for (j = 0; j < 8; j++) {
        setTextXY(j, 0);
        {
            for (i = 0; i < 16; i++) { //clear all columns
                putChar(' ');
            }
        }
    }
    send_command(OLED_Display_On_Cmd);    //display on
    setTextXY(0, 0);
}

void Oled::sendData(unsigned char Data) {
	unsigned char buff[] = {OLED_Data_Mode, Data};
	i2c.master_write(buff, 2, OLED_Address);
}

void Oled::sendMoreData(unsigned char * data, int length) {
	unsigned char buff[length+1];
	buff[0] = OLED_Data_Mode & Continuation_Bit;
	buff[1] = *data;
	i2c.master_write(buff, length, OLED_Address);
}

void Oled::putString(const char* String) {
    unsigned char i = 0;
    while (String[i]) {
        putChar(String[i]);
        i++;
    }
}

unsigned char Oled::putNumber(long long_num) {
    unsigned char char_buffer[10] = "";
    unsigned char i = 0;
    unsigned char f = 0;

    if (long_num < 0) {
        f = 1;
        putChar('-');
        long_num = -long_num;
    } else if (long_num == 0) {
        f = 1;
        putChar('0');
        return f;
    }

    while (long_num > 0) {
        char_buffer[i++] = long_num % 10;
        long_num /= 10;
    }

    f = f + i;
    for (; i > 0; i--) {
        putChar('0' + char_buffer[i - 1]);
    }
    return f;

}

unsigned char Oled::putFloat(float floatNumber, unsigned char decimal) {
    unsigned int temp = 0;
    float decy = 0.0;
    float rounding = 0.5;
    unsigned char f = 0;
    if (floatNumber < 0.0) {
        putString("-");
        floatNumber = -floatNumber;
        f += 1;
    }
    for (unsigned char i = 0; i < decimal; ++i) {
        rounding /= 10.0;
    }
    floatNumber += rounding;

    temp = floatNumber;
    f += putNumber(temp);
    if (decimal > 0) {
        putChar('.');
        f += 1;
    }
    decy = floatNumber - temp; //decimal part,
    for (unsigned char i = 0; i < decimal; i++) { //4
        decy *= 10; // for the next decimal
        temp = decy;//get the decimal
        putNumber(temp);
        decy -= temp;
    }
    f += decimal;
    return f;
}

unsigned char Oled::putFloat(float floatNumber) {
    unsigned char decimal = 2;
    unsigned int temp = 0;
    float decy = 0.0;
    float rounding = 0.5;
    unsigned char f = 0;
    if (floatNumber < 0.0) {
        putString("-");
        floatNumber = -floatNumber;
        f += 1;
    }
    for (unsigned char i = 0; i < decimal; ++i) {
        rounding /= 10.0;
    }
    floatNumber += rounding;

    temp = floatNumber;
    f += putNumber(temp);
    if (decimal > 0) {
        putChar('.');
        f += 1;
    }
    decy = floatNumber - temp; //decimal part,
    for (unsigned char i = 0; i < decimal; i++) { //4
        decy *= 10; // for the next decimal
        temp = decy;//get the decimal
        putNumber(temp);
        decy -= temp;
    }
    f += decimal;
    return f;
}

void Oled::drawBitmap(unsigned char* bitmaparray, int bytes) {
    char localAddressMode = addressingMode;
    if (addressingMode != HORIZONTAL_MODE) {
        //Bitmap is drawn in horizontal mode
        setHorizontalMode();
    }

    for (int i = 0; i < bytes; i++) {
        sendData(bitmaparray[i]);
    }

    if (localAddressMode == PAGE_MODE) {
        //If pageMode was used earlier, restore it.
        setPageMode();
    }

}

void Oled::setHorizontalScrollProperties(unsigned char direction, unsigned char startPage, unsigned char endPage,
        unsigned char scrollSpeed) {
    /*
        Use the following defines for 'direction' :

        Scroll_Left
        Scroll_Right

        Use the following defines for 'scrollSpeed' :

        Scroll_2Frames
        Scroll_3Frames
        Scroll_4Frames
        Scroll_5Frames
        Scroll_25Frames
        Scroll_64Frames
        Scroll_128Frames
        Scroll_256Frames

    */

    if (Scroll_Right == direction) {
        //Scroll Right
        send_command(0x26);
    } else {
        //Scroll Left
        send_command(0x27);

    }
    send_command(0x00);
    send_command(startPage);
    send_command(scrollSpeed);
    send_command(endPage);
    send_command(0x00);
    send_command(0xFF);
}

void Oled::activateScroll() {
    send_command(OLED_Activate_Scroll_Cmd);
}

void Oled::deactivateScroll() {
    send_command(OLED_Dectivate_Scroll_Cmd);
}

void Oled::setNormalDisplay() {
    send_command(OLED_Normal_Display_Cmd);
}

void Oled::setInverseDisplay() {
    send_command(OLED_Inverse_Display_Cmd);
}

void Oled::copy_frame(unsigned char * target_frame, unsigned char * source_frame) {
	for (int i = 0; i < FRAME_SIZE; ++i) {
		target_frame[i] = *(source_frame+i);
	}
} //copy frame()


void Oled::charge_pump_on(void) {
	send_command(0x8D);
	send_command(0x14);
} //charge_pump_on()


void Oled::charge_pump_off(void) {
	send_command(0x8D);
	send_command(0x10);
} //charge_pump_off()


void Oled::display_enable(void) {
	send_command(OLED_Display_On_Cmd);  //0xAF
} //display_enable()


void Oled::display_disable(void) {
	send_command(OLED_Display_Off_Cmd);  //0xAE
} //display_enable()


void Oled::set_mux_ratio(uint8_t ratio) {
	send_command(OLED_Set_Mux_Ratio_Cmd);
	send_command(ratio);
} //set_mux_ratio()


void Oled::set_vertical_shift(uint8_t pixels) {
	send_command(OLED_Set_Vertical_Shift_Cmd);
	send_command(pixels);
} //set_vertical_shift()


void Oled::set_display_ram_start_line(uint8_t line) {
	send_command(0x40 + line);
} //set_display_ram_start_line()


void Oled::set_segment_remap(bool remap_enable) {
	if (remap_enable == false) {
		send_command(OLED_Set_Seg_Remap_Disable_Cmd);
	} else {
		send_command(OLED_Set_Seg_Remap_Enable_Cmd);
	}
} //set_segment_remap()


void Oled::set_com_remap(bool remap_enable) {
	if (remap_enable == false) {
		send_command(OLED_Set_Com_Remap_Disable_Cmd);
	} else {
		send_command(OLED_Set_Com_Remap_Enable_Cmd);
	}
} //set_com_remap()


void Oled::set_brightness(uint8_t brightness) {
	send_command(OLED_Set_Brightness_Cmd);
	send_command(brightness);
} //set_contrast()


void Oled::all_pixels_on(void) {
	send_command(OLED_All_Pixels_On_Cmd);
} //all_pixels_on()


void Oled::display_ram_contents(void) {
	send_command(OLED_Display_Ram_Contents_Cmd);
} //display_ram_contents()


void Oled::set_inverse_colors(bool inversed_colors) {
	if (inversed_colors == false) {
		send_command(OLED_Normal_Display_Cmd);
	} else {
		send_command(OLED_Inverse_Display_Cmd);
	}
} //set_inverse_colors()

void Oled::set_clock_divider(uint8_t divider) {
	send_command(OLED_Set_Clock_Divider_Cmd);
	send_command(divider);
} //set_clock_divider()

void Oled::set_addresing_mode(uint8_t mode) {
	send_command(OLED_Set_Addr_Mode_Cmd);
	send_command(mode);
} //set_addresing_mode()
