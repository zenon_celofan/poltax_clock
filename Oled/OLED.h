#include "bluepill.h"
#include "I2C.h"


#ifndef OLED_H
#define OLED_H


#define HORIZONTAL_MODE                 0x00
#define VERTICAL_MODE                   0x01
#define PAGE_MODE						0x02


#define OLED_Address               		0x78
#define OLED_Command_Mode          		0x80
#define OLED_Data_Mode           		0x40
#define OLED_Display_Off_Cmd      		0xAE
#define OLED_Display_On_Cmd      		0xAF
#define OLED_Set_Addr_Mode_Cmd			0x20
#define OLED_Display_Ram_Contents_Cmd	0xA4
#define OLED_All_Pixels_On_Cmd			0xA5
#define OLED_Normal_Display_Cmd   		0xA6
#define OLED_Inverse_Display_Cmd  		0xA7
#define OLED_Activate_Scroll_Cmd   		0x2F
#define OLED_Dectivate_Scroll_Cmd  		0x2E
#define OLED_Set_Brightness_Cmd    		0x81
#define OLED_Set_Mux_Ratio_Cmd			0xA8
#define OLED_Set_Vertical_Shift_Cmd		0xD3
#define OLED_Set_Clock_Divider_Cmd		0xD5
#define OLED_Set_Seg_Remap_Disable_Cmd	0xA0
#define OLED_Set_Seg_Remap_Enable_Cmd	0xA1
#define OLED_Set_Com_Remap_Disable_Cmd	0xC0
#define OLED_Set_Com_Remap_Enable_Cmd	0xC8
#define OLED_MUX64						0x3F


#define Continuation_Bit				0b10000000

#define SCREEN_SIZE				4096  // 128*32
#define FRAME_SIZE				(SCREEN_SIZE/4+1)
#define CONTRAST_THRESHOLD		1 //0x7F

#define Scroll_Left             0x00
#define Scroll_Right            0x01

#define Scroll_2Frames          0x7
#define Scroll_3Frames          0x4
#define Scroll_4Frames          0x5
#define Scroll_5Frames          0x0
#define Scroll_25Frames         0x6
#define Scroll_64Frames         0x1
#define Scroll_128Frames        0x2
#define Scroll_256Frames        0x3


class Oled {

public:
	I2C i2c;

	unsigned char current_frame[1025];  // frame size = screen size / 4
	unsigned char next_frame[1025];

    char addressingMode;

    Oled(I2C_TypeDef* _i2c);
    void init(void);

    void setNormalDisplay();
    void setInverseDisplay();

    void send_command(unsigned char command);
    void sendData(unsigned char Data);
    void sendMoreData(unsigned char * data, int length);

    void setPageMode();
    void setHorizontalMode();

    void setTextXY(unsigned char Row, unsigned char Column);
    void clearDisplay();
    void setBrightness(unsigned char Brightness);
    void putChar(unsigned char c);
    void putString(const char* String);
    unsigned char putNumber(long n);
    unsigned char putFloat(float floatNumber, unsigned char decimal);
    unsigned char putFloat(float floatNumber);
    void drawBitmap(unsigned char* bitmaparray, int bytes);

    void set_pixel(unsigned char x, unsigned char y, unsigned char status);
    void refresh(void);
    void refresh(unsigned char table[]);

    void scroll_up(unsigned char step = 1);
    void scroll_down(unsigned char step = 1);
    void scroll_random(unsigned char step = 1);

    void setHorizontalScrollProperties(unsigned char direction, unsigned char startPage, unsigned char endPage,
                                       unsigned char scrollSpeed);
    void activateScroll();
    void deactivateScroll();

    void copy_frame(unsigned char * target_frame, unsigned char * source_frame);

    void charge_pump_on(void);
    void charge_pump_off(void);
    void display_enable(void);
    void display_disable(void);
    void set_mux_ratio(uint8_t ratio = 0x3F);  //MUX64
    void set_vertical_shift(uint8_t pixels = 0x00);
    void set_display_ram_start_line(uint8_t line = 0x00);
    void set_segment_remap(bool remap_enable = false);
    void set_com_remap(bool remap_enable = false);
    void set_brightness(uint8_t contrast = 0x7F); //Half brightness value
    void all_pixels_on(void);
    void display_ram_contents(void);
    void set_inverse_colors(bool inversed_colors = false);
    void set_clock_divider(uint8_t divider = 0x00);  //Reset=0x00, better visual effect=0x80
    void set_addresing_mode(uint8_t mode = HORIZONTAL_MODE);
};

#endif
